$(document).ready(function() {
	$("#btnCount").click(function() {
		countWords();
	});
});

function countWords() {
	$.ajax({
		url : "ShowResult",
		type : "POST",
		dataType : "json",
		data : {
			"content" : $("#content").val()
		},
		success : function(data) {
			$("#result").html("Words: " + data.count).fadeIn();
		}
	});
}